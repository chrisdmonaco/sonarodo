/********************************COPYRIGHT CHRIS D. MONACO, 2019**********************************/
#pragma once

#include <string>

#include "ros/ros.h"
#include "ros/time.h"
#include "sonar_msgs/Sonar.h"
#include "std_msgs/Header.h"
#include "geometry_msgs/Quaternion.h"

#include "sonarodo/spatial_image_parameters.h"
#include "sonarodo/trans_vel_estimator.h"
#include "sonarodo/rot_vel_estimator.h"

/**
 * @brief Class to estimate a Forward-Looking SONAR's ego-motion via the "SONARODO" algorithm.
 *        See "Motion Estimation from Doppler and Spatial Data in SONAR Images"
 *        (Monaco et al., 2019)
 * 
 */
class Sonarodo {
 public:
  Sonarodo();

  /**
   * @brief Visualizes an image in an image window with the option to convert it to an equivalent 
   *        colormap
   * 
   * @param[in] image                   Image to visualize
   * @param[in] image_window_name       Image window's name
   * @param[in] convert_to_colormap     Boolean flag indicating whether to convert image to a
   *                                      colormap
   * @param[in] delay_ms                Delay [ms] between displayed images in the image window
   */
  static void Visualize(const cv::Mat& image,
                        const std::string& image_window_name,
                        const bool convert_to_colormap = false,
                        const int delay_ms = 1);

 private:
  /**
   * @brief Callback function for when the SONAR publishes its measurements
   * 
   * @param[in] sonar_msg_ptr   Pointer to the SONAR's measurement message
   */
  void SonarDataCallback(const sonar_msgs::SonarConstPtr sonar_msg_ptr);

  /**
   * @brief Initializes the Sonarodo object
   * 
   * @param[in] sonar_msg                       SONAR measurement message
   * @param[in] polar_image                     CV_16UC1 SONAR Range-Azimuth (polar) image
   * @param[in] cartesian_image_aspect_ratio    Aspect ratio of equivalent cartesian image; bigger
   *                                              ratios increase width relative to height 
   *                                              (default: 1)
   */
  void Initialize(const sonar_msgs::Sonar& sonar_msg,
                  const cv::Mat& polar_image,
                  const double cartesian_image_aspect_ratio = 1.0);

  /**
   * @brief Finds minimum and maximum detectable ranges in a Range-Azimuth SONAR image by analyzing
   *        their signal intensities
   * 
   * @param[in] polar_image_params        Polar image parameters
   * @param[in] polar_image               CV_16UC1 Range-Azimuth (polar) SONAR image
   * @param[out] min_detectable_range     Minimum detectable range [m]
   * @param[out] max_detectable_range     Maximum detectable range [m]
   * @return uint                         v- pixel coordinate corresponding to minimum range
   */
  uint FindDetectableRanges(const PolarImageParameters& polar_image_params,
                            const cv::Mat& polar_image,
                            double& min_detectable_range,
                            double& max_detectable_range);

  /**
   * @brief Transforms quaternion into three-element vector of equivalent Euler angles
   * 
   * @param[in] _quaternion            Quaternion
   * @param[out] euler_angles_rad      Three-element vector of equivalent Euler angles [rad]
   *                                     reported in x, y, z order
   */
  void ExtractEulerAngles(const geometry_msgs::Quaternion& _quaternion,
                          Eigen::Vector3d& euler_angles_rad);

  /**
   * @brief Resizes an image accoring to image width and height scale factors
   * 
   * @param[in] original_image          Original image
   * @param[in] width_scale_factor      Image width scale factor
   * @param[in] height_scale_factor     Image height scale factor
   * @param[out] resized_image          Resized image
   */
  void ResizeImage(const cv::Mat& original_image,
                   const double width_scale_factor,
                   const double height_scale_factor,
                   cv::Mat& resized_image);

  /**
   * @brief Publishes the 2D Forward-Looking SONAR's ego-motion estimate over ROS
   * 
   * @param[in] translational_velocity_estimate     Two-element vector of the SONAR's translational
   *                                                  ego-motion estimate [v_x v_y] [m/s]
   * @param[in] yaw_rate_estimate                   SONAR's yaw rate estimate [rad/s]
   * @param[in] msg_header                          Current SONAR measurement's ROS message header
   */
  void PublishEgomotionEstimate(const Eigen::Vector2d& translational_velocity_estimate,
                                const double yaw_rate_estimate,
                                const std_msgs::Header& msg_header);

  const double polar_image_height_scale_factor_;    // Resized polar image height's scale factor
  const double polar_image_width_scale_factor_;     // Resized polar image width's scale factor
  const double cartesian_image_aspect_ratio_;       // Aspect ratio of equivalent cartesian image
  bool initialized_;                                // Boolean flag if object is initialized
  ros::NodeHandle nh_;                              // ROS Nodehandle
  ros::Subscriber sonar_data_subscriber_;           // ROS 2D Forward-Looking SONAR measurement
                                                    //   subscriber
  ros::Publisher sonar_egomotion_publisher_;        // ROS Publisher to publish SONAR ego-motion
                                                    //   estimates
  PolarImageParameters polar_image_params_;         // Resized polar image parameters
  TransVelEstimator trans_vel_estimator_;           // Doppler-based Translational Velocity
                                                    //   Estimator
  RotVelEstimator rot_vel_estimator_;               // Spatial data-based Rotational Velocity
                                                    //   Estimator
  ros::Time previous_msg_timestamp_;                // Previous SONAR measurement message's
                                                    //   timestamp
};
