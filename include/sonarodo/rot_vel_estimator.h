/********************************COPYRIGHT CHRIS D. MONACO, 2019**********************************/
#pragma once

#include <opencv2/core/core.hpp>
#include <eigen3/Eigen/Core>

#include <sonarodo/spatial_image_parameters.h>

/**
 * @brief Class that estimates the SONAR's rotational velocities largely from Range-Azimuth images
 * 
 */
class RotVelEstimator {
 public:
  /**
   * @brief Construct a new RotVelEstimator object
   * 
   */
  RotVelEstimator();

  /**
   * @brief Reports if the RotVelEstimator object was intialized
   * 
   * @return true     If initialized
   * @return false    Otherwise
   */
  inline bool is_initialized() { return initialized_; }

  /**
   * @brief Initializes the RotVelEstimator object
   * 
   * @param[in] current_polar_image           Curent CV_16UC1 Range-Azimuth (polar) SONAR image
   * @param[in] polar_image_parameters        Polar SONAR image parameters
   * @param[in] cartesian_image_parameters    Cartesian SONAR image parameters
   */
  void Initialize(const cv::Mat& current_polar_image,
                  const PolarImageParameters& polar_image_parameters,
                  const CartesianImageParameters& cartesian_image_parameters);

  /**
   * @brief Stores SONAR polar image for use during the next iteration
   * 
   * @param[in] polar_image     Curent CV_16UC1 Range-Azimuth (polar) SONAR image 
   */
  inline void StoreImage(const cv::Mat& polar_image) {previous_polar_image_ = polar_image.clone();}


  /**
   * @brief Remaps an image from one coordinate system to another
   * 
   * @tparam SrcCoordSysParams  Parameters of the source image's coordinate system
   * @tparam DstCoordSysParams  Parameters of the destination image's coordinate system
   * @param[in] src_image       Source image
   * @param[in] dst_image       Destination image
   */
  template<typename SrcCoordSysParams, typename DstCoordSysParams>
  void Remap(const cv::Mat& src_image, cv::Mat& dst_image);

  /**
   * @brief Estimates the SONAR's yaw rate from SONAR Range-Azimuth (polar) images
   * 
   * @param[in] current_polar_image              Current CV_16UC1 SONAR Range-Azimuth (polar) image
   * @param[in] translational_velocity_estimate  SONAR's translational velocity estimate [v_x, v_y]
   *                                               [m/s]
   * @param[in] delta_t                          Time between consecutive image measurements [sec.]
   * @param[in] min_detectable_range_image_v     Image v-coordinate of the minimum detectable range
   *                                               in the polar SONAR image
   * @return double                              SONAR yaw rate estimate [rad/s]
   */
  double Estimate(const cv::Mat& current_polar_image,
                  const Eigen::Vector2d& translational_velocity_estimate,
                  const double delta_t,
                  const uint min_detectable_range_image_v);

 private:
  /**
   * @brief Generates u- and v- pixel maps required to remap images
   * 
   * @tparam SrcCoordSysParams    Parameters of the source image's coordinate system
   * @tparam DstCoordSysParams    Parameters of the destination image's coordinate system
   * @param[in] src_image_params  Parameters of the source image's coordinate system
   * @param[in] dst_image_params  Parameters of the destination image's coordinate system
   * @param[out] src2dst_u_map    CV_32FC1 u-coordinate pixel map to remap source image to 
   *                                destination image
   *                                (Contains corresponding pixel coordinates of the source image
   *                                 for each destination image pixel)
   * @param[out] src2dst_v_map    CV_32FC1 v-coordinate pixel map to remap source image to 
   *                                destination image
   *                                (Contains corresponding pixel coordinates of the source image
   *                                 for each destination image pixel)
   */
  template<typename SrcCoordSysParams, typename DstCoordSysParams>
  void GenerateMap(const SrcCoordSysParams& src_image_params,
                   const DstCoordSysParams& dst_image_params,
                   cv::Mat& src2dst_u_map,
                   cv::Mat& src2dst_v_map);

  /**
   * @brief Generates images whose pixel values correspond to their real-world unit values for their
   *        image u-coordinates and v-coordinates
   * 
   * @param[in] image_params            Spatial image parameters
   * @param[out] u_unit_values_image    CV_32FC1 unit values image for image's u-coordinates
   * @param[out] v_unit_values_image    CV_32FC1 unit values image for image's v-coordinates
   */
  void GenerateUnitValueImages(const SpatialImageParameters& image_params,
                               cv::Mat& u_unit_values_image,
                               cv::Mat& v_unit_values_image);

  /**
   * @brief Generates vectors that contains the real-world unit values corresponding to either
   *        the image's u- or v- image coordinates
   * 
   * @param[in] image_params          Spatial image parameters
   * @param[in] u_unit_values_vector  Unit values vector corresponding to the image's u-coordinates
   * @param[in] v_unit_values_vector  Unit values vector corresponding to the image's v-coordinates
   */
  void GenerateUnitValuesVectors(
    const SpatialImageParameters& image_params,
    Eigen::Array<float, Eigen::Dynamic, 1>& u_unit_values_vector,
    Eigen::Array<float, Eigen::Dynamic, 1>& v_unit_values_vector);

  /**
   * @brief Generates images that correspond to unit value vectors. In other words, image
   *        pixels contain real-world unit values corresponding to their u or v image coordinate.
   * 
   * @param[in] u_unit_values_vector          Unit values vector corresponding to the image's 
   *                                            u-coordinates
   * @param[in] v_unit_values_vector          Unit values vector corresponding to the image's 
   *                                            v-coordinates
   * @param[out] u_unit_values_image_vector   CV_32FC1 image vector that contains real-world unit 
   *                                            values for the image's u coordinates
   * @param[out] v_unit_values_image_vector   CV_32FC1 image vector that contains real-world unit 
   *                                            values for the image's u coordinates
   */
  void GenerateUnitValuesImageVectors(
    Eigen::Array<float, Eigen::Dynamic, 1>& u_unit_values_vector,
    Eigen::Array<float, Eigen::Dynamic, 1>& v_unit_values_vector,
    cv::Mat& u_unit_values_image_vector,
    cv::Mat& v_unit_values_image_vector);

  /**
   * @brief Calculates pixel conversion map for image remapping 
   *        (overloaded for polar to cartesian image conversion)
   * 
   * @param[in] dst_cart_y_values_image   CV_32FC1 image containing the destination cartesian 
   *                                        image's y values
   * @param[in] dst_cart_x_values_image   CV_32FC1 image containing the destination cartesian 
   *                                        image's x values
   * @param[in] src_polar_image_params    Source polar image parameters
   * @param[out] polar2cart_u_map         CV_32FC1 Ppxel u-coordinate map required to remap polar 
   *                                        images to their cartesian equivalent (size of cartesian 
   *                                        image, holds corresponding image coordinates in the 
   *                                        polar image)
   * @param[out] polar2cart_v_map         CV_32FC1 pixel v-coordinate map required to remap polar 
   *                                        images to their cartesian equivalent (size of cartesian
   *                                        image, holds corresponding image coordinates in the
   *                                        polar image)
   */
  void CalcPixelConversionMap(const cv::Mat& dst_cart_y_values_image,
                              const cv::Mat& dst_cart_x_values_image,
                              const PolarImageParameters& src_polar_image_params,
                              cv::Mat& polar2cart_u_map,
                              cv::Mat& polar2cart_v_map);

  /**
   * @brief Calculates pixel conversion map for image remapping 
   *        (overloaded for polar to cartesian image conversion)
   * 
   * @param[in] dst_azimuth_rad_values_image  CV_32FC1 image containing the destination polar 
   *                                            image's  azimuth angle values [rad]
   * @param[in] dst_range_values_image        CV_32FC1 image containing the destination polar 
   *                                            image's  range values [m]
   * @param[in] src_cart_image_params         Source cartesian image parameters
   * @param[out] cart2polar_u_map             CV_32FC1 pixel u-coordinate map required to remap  
   *                                            cartesian images to their polar equivalent (size 
   *                                            of polar image, holds corresponding image 
   *                                            coordinates in  the cartesian image)
   * @param[out] cart2polar_v_map             CV_32FC1 pixel v-coordinate map required to remap 
   *                                            cartesian  images to their polar equivalent (size 
   *                                            of polar image, holds corresponding image 
   *                                            coordinates in the cartesian image)
   */
  void CalcPixelConversionMap(const cv::Mat& dst_azimuth_rad_values_image,
                              const cv::Mat& dst_range_values_image,
                              const CartesianImageParameters& src_cart_image_params,
                              cv::Mat& cart2polar_u_map,
                              cv::Mat& cart2polar_v_map);


  /**
   * @brief Generate images that contain cosine and sine values corresponding to the polar image's
   *        azimuth values
   * 
   * @param[in] azimuth_values_image_vector         CV_32FC1 image vector that contains azimuth 
   *                                                  values [rad]
   * @param[in] polar_image_height                  Polar image height
   * @param[out] polar_cos_azimuth_values_image     CV_32FC1 image that contains the cosine values
   *                                                  that correspond to the pixels' azimuths
   * @param[out] polar_sin_azimuth_values_image     CV_32FC1 image that contains the sine values
   *                                                  that correspond to the pixels' azimuths
   */
void GenerateCosSinAzimuthImages(const cv::Mat& azimuth_values_image_vector,
                                 const uint polar_image_height,
                                 cv::Mat& polar_cos_azimuth_values_image,
                                 cv::Mat& polar_sin_azimuth_values_image);

  /**
   * @brief Shifts an image so its top-left corner is at the image center (or vice versa).
   *        This is critical for cyclic cross-power spectrum images to shift their low-frequency
   *        components to the image center (or vice versa).
   * 
   * @param[in, out] image    Image to be shifted
   */
  void FFTShift(cv::Mat& image);

  /**
   * @brief Remaps the previous polar image corresponding to the current translational velocity
   *        estimate
   * 
   * @param[in] previous_polar_image                Previous CV_16UC1 Range-Azimuth (polar) image
   * @param[in] translational_velocity_estimate     Two-element translational ego-motion estimate
   *                                                  vector [v_x, v_y] [m/s]
   * @param[in] delta_t                             Time between images [sec]
   * @param[in] polar_image_params                  Polar image parameters
   * @param[in] polar_range_values_image            CV_32FC1 image whose pixels contain the range
   *                                                  values [m] for the polar image
   * @param[in] polar_azimuth_values_image          CV_32FC1 image whose pixels contain the azimuth
   *                                                  values [rad] for the polar image
   * @param[in] polar_cos_azimuth_values_image      CV_32FC1 image whose pixels contain the cosine
   *                                                  of the azimuth values for the polar image
   * @param[in] polar_sin_azimuth_values_image      CV_32FC1 image whose pixels contain the sine
   *                                                  of the azimuth values for the polar image
   * @param[out] remapped_previous_polar_image      CV_16UC1 image that is the previous polar image
   *                                                  remapped accoding to the current
   *                                                  translational ego-motion estimate
   */
  void RemapPreviousPolarImage(const cv::Mat& previous_polar_image,
                               const Eigen::Vector2d& translational_velocity_estimate,
                               const double delta_t,
                               const PolarImageParameters& polar_image_params,
                               const cv::Mat& polar_range_values_image,
                               const cv::Mat& polar_azimuth_values_image,
                               const cv::Mat& polar_cos_azimuth_values_image,
                               const cv::Mat& polar_sin_azimuth_values_image,
                               cv::Mat& remapped_previous_polar_image);

  /**
   * @brief Finds the perceived azimuth change of the environment that corresponds to the SONAR's
   *        yaw rate
   * 
   * @param[in] remapped_previous_polar_image   CV_16UC1 image that is the previous polar image
   *                                                  remapped accoding to the current
   *                                                  translational ego-motion estimate
   * @param[in] current_polar_image             CV_16UC1 current Range-Azimuth (polar) image
   * @param[in] polar_image_params              Polar image parameters
   * @param[in] min_detectable_range_image_v    Image v-coordinate that corresponds to the minimum
   *                                              detectable range in the Range-Azimuth image
   * @return double                             The perceived azimuth change [rad] of the 
   *                                              environment that corresponds to the SONAR's yaw
   *                                              rate
   */
  double FindAzimuthChange(const cv::Mat& remapped_previous_polar_image,
                           const cv::Mat& current_polar_image,
                           const PolarImageParameters& polar_image_params,
                           const uint min_detectable_range_image_v);

  /**
   * @brief Generate images that represent the Discrete Fourier Transforms (DFTs) of the current
   *        and previous remapped polar images. 
   * 
   * @param[in] previous_remapped_polar_spatial_image       Previous remapped polar image 
   *                                                          (pixels correspond to bins in space)
   * @param[in] current_polar_spatial_image                 Current polar image 
   *                                                          (pixels correspond to bins in space)
   * @param[in] region_of_interest                          CV_8UC1 ``boolean" that represents the
   *                                                          region of interest (rejects padding
   *                                                          pixels and ranges less than min.
   *                                                          range). True (255) values correspond
   *                                                          to pixels in the ROI
   * @param[out] previous_remapped_polar_frequency_image    2-channel DFT output image that
   *                                                          contains the frequency
   *                                                          components of the previous remapped
   *                                                          polar image
   * @param[out] current_polar_frequency_image              2-channel DFT output image that
   *                                                          contains the frequency
   *                                                          components of the current polar
   *                                                          image
   */
  void GenerateDFTImages(const cv::Mat& previous_remapped_polar_spatial_image,
                         const cv::Mat& current_polar_spatial_image,
                         const cv::Rect& region_of_interest,
                         cv::Mat& previous_remapped_polar_frequency_image,
                         cv::Mat& current_polar_frequency_image);

  /**
   * @brief Generate "prepped" spatial images (i.e. pad the images to the optimal DFT size and
   *        filter them with a Hann function)
   * 
   * @param[in] polar_spatial_image             CV_16UC1 Range-Azimuth (polar) image
   * @param[in] region_of_interest              CV_8UC1 ``boolean" image that marks the region
   *                                              of interest (255 indicates pixels in the ROI)
   * @param[in] hann_window                     Hann filter window image used to attentuate edges
   * @param[out] prepped_polar_spatial_image    "Prepped" Range-Azimuth polar image where pixels
   *                                              correspond to bins in space (i.e. images are 
   *                                              padded to the optimal DFT size and filtered
   *                                              with a Hann function)
   */
  void GeneratePreppedSpatialImage(const cv::Mat& polar_spatial_image,
                                   const cv::Rect& region_of_interest,
                                   const cv::Mat& hann_window,
                                   cv::Mat& prepped_polar_spatial_image);

  /**
   * @brief Calculates the normalized cross-power spectrum image and then generates its phase 
   *        correlation matrix via the Inverse Discrete Fourier Transform
   * 
   * @param[in] previous_remapped_polar_frequency_image   Image that contains the frequency
   *                                                        components of the previous remapped
   *                                                        polar image
   * @param[in] current_polar_frequency_image             Image that contains the frequency
   *                                                        components of the current polar image
   * @param[in] region_of_interest_height                 Height [pixels] of the polar images'
   *                                                        region of interest
   * @param[out] idft_image_row                           First row of the resulting phase
   *                                                        correlation image (generated via the
   *                                                        IDFT) since the first row corresponds
   *                                                        to a zero range offset
   */
  void GenerateIDFTImage(const cv::Mat& previous_remapped_polar_frequency_image,
                         const cv::Mat& current_polar_frequency_image,
                         const uint region_of_interest_height,
                         cv::Mat& idft_image_row);

  /**
   * @brief Refines the peak location to sub-pixel resolution by calculating its weighted centroid
   * 
   * @param[in] idft_image_row    First row of the phase correlation image (result of an IDFT)
   * @param[in,out] peak_point    Point of the idft_image_rows peak. Peak is changed to a sub-pixel
   *                                resolution in this function
   */
  void RefinePeakLocation(const cv::Mat& idft_image_row,
                        cv::Point2d& peak_point);

  bool initialized_;                                      // Boolean flag that indicates if
                                                          //   initialized
  PolarImageParameters polar_image_parameters_;           // Polar image parameters
  CartesianImageParameters cartesian_image_parameters_;   // Cartesian image parameters
  cv::Mat polar_range_values_image_;                      // Image that contains range values for
                                                          //   corresponding polar image pixels
  cv::Mat polar_azimuth_values_image_;                    // Image that contains azimuth [rad]
                                                          //   values for corresponding polar
                                                          //   image pixels
  cv::Mat polar_cos_azimuth_values_image_;                // Image that contains cosine values
                                                          //  for polar image azimuth values
  cv::Mat polar_sin_azimuth_values_image_;                // Image that contains sine values
                                                          //  for polar image azimuth values
  cv::Size optimal_dft_image_size_;                       // Size of the polar image's optimal
                                                          //   DFT size
  cv::Mat high_frequency_attenuation_window_;             // Image filter to attentuate high-freq.
                                                          //  components of a cross-power spectrum
                                                          //  (shifted Hann function)
  cv::Mat previous_polar_image_;                          // The previous polar image
  cv::Mat polar2cart_u_map_;                              // Image u-coord. map to remap from a
                                                          //   polar image to a cartesian image
  cv::Mat polar2cart_v_map_;                              // Image v-coord. map to remap from a
                                                          //   polar image to a cartesian image
  cv::Mat cart2polar_u_map_;                              // Image u-coord. map to remap from a
                                                          //   cartesian image to a polar image
  cv::Mat cart2polar_v_map_;                              // Image v-coord. map to remap from a
                                                          //   cartesian image to a polar image
};

// TEMPLATE SPECIALIZATION DECLARATIONS:
template<>
void RotVelEstimator::Remap<PolarImageParameters, CartesianImageParameters>(
    const cv::Mat& src_polar_image, cv::Mat& dst_cart_image);

template<>
void RotVelEstimator::Remap<CartesianImageParameters, PolarImageParameters>(
    const cv::Mat& src_cart_image, cv::Mat& dst_polar_image);
