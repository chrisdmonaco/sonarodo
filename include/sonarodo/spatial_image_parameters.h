/********************************COPYRIGHT CHRIS D. MONACO, 2019**********************************/
#pragma once

#include <opencv2/core/core.hpp>

/**
 * @brief Structure to contain spatial image parameters (images whose pixels correspond to bins 
 *        in space)
 * 
 */
struct SpatialImageParameters {
  SpatialImageParameters() { }

  /**
   * @brief Construct a new SpatialImageParameters object
   * 
   * @param[in] _width        Image width [pixels]
   * @param[in] _height       Image height [pixels]
   * @param[in] _u0           u-pixel location of coord. system's image origin
   * @param[in] _v0           v-pixel location of coord. system's image origin
   * @param[in] _dunit_du     Partial derivative of the image's real-world unit with respect to its
   *                            u- image coordinate
   * @param[in] _dunit_dv     Partial derivative of the image's real-world unit with respect to its
   *                            u- image coordinate
   */
  SpatialImageParameters(const uint _width,
               const uint _height,
               const double _u0,
               const double _v0,
               const double _dunit_du,
               const double _dunit_dv)
      : width(_width),
        height(_height),
        u0(_u0),
        v0(_v0),
        dunit_du(_dunit_du),
        dunit_dv(_dunit_dv) { }

  /**
   * @brief Get real-world unit value corresponding to pixel location on one of the two
   *        image axes
   * 
   * @param[in] pixel_coordinate    Pixel coordinate on one of the two image axes (u or v coord.)
   * @param[in] dimension           Order of image dimension (0 corresponds to the first 
   *                                  image axis, u; 1 corresponds to v)
   * @return double                 Corresponding real-world unit value
   */
  inline double GetUnitValue(const double pixel_coordinate, const uint dimension) const {
    return dimension ? (pixel_coordinate - v0) * dunit_dv : (pixel_coordinate - u0) * dunit_du;
  }

  /**
   * @brief Get the pixel coordinate (about one of the image axes) corresponding to a real-world
   *        unit value
   * 
   * @param[in] unit_value      Real-world unit value
   * @param[in] dimension       Order of image dimension (0 corresponds to the first 
   *                                  image axis, u; 1 corresponds to v)
   * @return double             Corresponding pixel coordinate (about one of the image axes)
   */
  inline double GetPixelCoordinate(const double unit_value,  const uint dimension) const {
    return dimension ? unit_value / dunit_dv + v0 : unit_value / dunit_du + u0;
  }

  uint width;           // Image width [pixels]
  uint height;          // Image height [pixels]
  double u0;            // u-pixel location of coord. system's image origin
  double v0;            // v-pixel location of coord. system's image origin
  double dunit_du;      // Partial derivative of the image's real-world unit with respect to its
                        //   u- image coordinate
  double dunit_dv;      // Partial derivative of the image's real-world unit with respect to its
                        //   v- image coordinate
};

// Inherited SpatialImageParameters structure for Cartesian images
struct CartesianImageParameters : public SpatialImageParameters {
  using SpatialImageParameters::SpatialImageParameters;
};

// Inherited SpatialImageParameters structure for Polar images
struct PolarImageParameters : public SpatialImageParameters {
    using SpatialImageParameters::SpatialImageParameters;
};
