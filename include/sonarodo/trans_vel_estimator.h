/********************************COPYRIGHT CHRIS D. MONACO, 2019**********************************/
#pragma once

#include <opencv2/core/core.hpp>
#include <eigen3/Eigen/Core>

/**
 * @brief Class to estimate the SONAR's translational velocities from Doppler-Azimuth images
 * 
 */
class TransVelEstimator {
 public:
  /**
   * @brief Construct a new TransVelEstimator object
   * 
   * @param[in] minimum_vehicle_speed_for_calc      Minimum vehicle speed required for a valid
   *                                                  ego-motion estimate calculation
   * @param[in] filtered_derivative_kernel_size     Size [pixels] of kernel used to both
   *                                                  both filter and calculate the derivative
   *                                                  of the Doppler-Azimuth image
   * @param[in] irls_iterations                     Iterations for Iterative Recursive Least
   *                                                  Squares estimation process
   */
  TransVelEstimator(const double minimum_vehicle_speed_for_calc,
                    const uint filtered_derivative_kernel_size = 7,
                    const uint irls_iterations = 5);

  /**
   * @brief Indicates if TransVelEstimator object has been initialized
   * 
   * @return true     If initialized
   * @return false    Otherwise
   */
  inline bool is_initialized() { return initialized_; }

  /**
   * @brief Initializes the TransVelEstimator object 
   * 
   * @param[in] doppler_image_width       Width of Doppler-Azimuth image [pixels]
   * @param[in] doppler_image_height      Height of Doppler-Azimuth image [pixels]
   * @param[in] min_azimuth_rad           SONAR's minimum azimuth [rad]
   * @param[in] max_azimuth_rad           SONAR's maximum azimuth [rad]
   * @param[in] min_range_rate            SONAR's min. detectable range rate [m/s]
   * @param[in] max_range_rate            SONAR's max. detectable range rate [m/s]
   * @param[in] sonar_depression_angle_rad_wrt_vehicle 
   *                                      SONAR's depression angle with respect to the vehicle's
   *                                        reference frame
   */
  void Initialize(const uint doppler_image_width,
                  const uint doppler_image_height,
                  const double min_azimuth_rad,
                  const double max_azimuth_rad,
                  const double min_range_rate,
                  const double max_range_rate,
                  const double sonar_depression_angle_rad_wrt_vehicle);

  /**
   * @brief Estimate's the SONAR's translational velocities largely using the Doppler-Azimuth image
   * 
   * @param[in] doppler_image           Doppler-Azimuth image
   * @param[in] min_detectable_range    Minimum detectable range [m] from the Range-Azimuth image
   * @param[in] max_detectable_range    Maximum detectable range [m] from the Range-Azimuth image
   * @param[in] echo_sounder_depth      Depth [m] to seafloor measured by an echo sounder aligned
   *                                      with the vehicle's z-axis
   * @param[in] vehicle_pitch           Vehicle's pitch [rad]
   * @param[in] a_priori_vehicle_trans_vel_estimate 
   *                                    Three-element vector of the vehicle's a priori
   *                                      translational velocity estimate [v_x v_y v_z] [m/s] 
   * @param[out] translational_velocity_estimate 
   *                                    Two-element vector of the 2D SONAR's translational 
   *                                      ego-motion estimate [v_x v_y] [m/s] 
   * @return true                       If ego-motion estimate is valid
   * @return false                      Otherwise
   */
  bool Estimate(const cv::Mat& doppler_image,
                const double min_detectable_range,
                const double max_detectable_range,
                const double echo_sounder_depth,
                const double vehicle_pitch,
                const Eigen::Vector3d& a_priori_vehicle_trans_vel_estimate,
                Eigen::Vector2d& translational_velocity_estimate);

  /**
   * @brief Estimates the depression angles of the SONAR's min. and max. range beamwidth
   *        edges as well as the depression angle of its a priori translational velocity
   *        estimate
   * 
   * @param[in] min_detectable_range      Min. detectable range [m] from the Range-Azimuth image
   * @param[in] max_detectable_range      Max. detectable range [m] from the Range-Azimuth image
   * @param[in] echo_sounder_depth        Depth [m] to seafloor measured by an echo sounder 
   *                                        aligned with the vehicle's z-axis
   * @param[in] vehicle_pitch             Vehicle pitch [rad]
   * @param[in] a_priori_vehicle_trans_vel_estimate 
   *                                      Three-element vector of the vehicle's a priori
   *                                        translational velocity estimate [v_x v_y v_z] [m/s] 
   *                                        (assumed to be roughly equal to the SONAR's)
   * @param[out] min_mag_return_depress_angle_wrt_vehicle 
   *                                      Minimum (in terms of magnitude) depression angle [rad]
   *                                        of beamwidth's edge with respect to the vehicle's
   *                                        coordinate system
   * @param[out] max_mag_return_depress_angle_wrt_vehicle 
   *                                       Maximum (in terms of magnitude) depression angle [rad]
   *                                        of beamwidth's edge with respect to the vehicle's
   *                                        coordinate system
   * @param[out] a_priori_vehicle_trans_vel_depression_angle 
   *                                       Depression angle [rad] of the a priori estimate of the
   *                                         vehicle's translational velocity with respect to the
   *                                         vehicle's coordinate system
   */
void EstimateDepressionAngles(const double min_detectable_range,
                              const double max_detectable_range,
                              const double echo_sounder_depth,
                              const double vehicle_pitch,
                              const Eigen::Vector3d& a_priori_vehicle_trans_vel_estimate,
                              double& min_mag_return_depress_angle_wrt_vehicle,
                              double& max_mag_return_depress_angle_wrt_vehicle,
                              double& a_priori_vehicle_trans_vel_depression_angle);

  /**
   * @brief Determines if a valid estimate is feasible. In this context, a valid estimate includes 
   *        both magnitude and directionality. Thus, the trans. vel.'s vector must reside OUTSIDE
   *        of the SONAR's beamwidth
   * 
   * @param[in] min_mag_return_depress_angle_wrt_vehicle
   *                                        Minimum (in terms of magnitude) depression angle [rad]
   *                                          of beamwidth's edge with respect to the vehicle's
   *                                          coordinate system
   * @param[in] max_mag_return_depress_angle_wrt_vehicle 
   *                                        Maximum (in terms of magnitude) depression angle [rad]
   *                                          of beamwidth's edge with respect to the vehicle's
   *                                          coordinate system
   * @param[in] a_priori_vehicle_trans_vel_depression_angle 
   *                                        Depression angle [rad] of the a priori estimate of the
   *                                          vehicle's translational velocity with respect to the
   *                                          vehicle's coordinate system
   * @return true                           If a valid estimate is feasible
   * @return false                          Otherwise
   */
  bool ValidEstimateFeasible(const double min_mag_return_depress_angle_wrt_vehicle,
                             const double max_mag_return_depress_angle_wrt_vehicle,
                             const double a_priori_vehicle_trans_vel_depression_angle);

  /**
   * @brief Determines if a number is within the provided range
   * 
   * @param boundary_num_1    The range's first boundary number
   * @param x                 Number to test if in range
   * @param boundary_num_2    The range's second boundary number
   * @return true             If number is in range
   * @return false            Otherwise
   */
  inline bool IsInRange(const double boundary_num_1, const double x, const double boundary_num_2) {
    return ((x - boundary_num_1) * (x - boundary_num_2) <= 0);
  }

  /**
   * @brief Extracts measurement vectors that contain data from the Doppler-Azimuth image's "edge"
   *        points
   * 
   * @param[in] doppler_image                   SONAR's Doppler-Azimuth image
   * @param[out] doppler_data_pt_range_rates    Vector that contains the range rates for each of
   *                                              Doppler-Azimuth image's "edge" points [m/s].
   *                                              Each index corresponds to the respective 
   *                                              azimuth value
   * @param[out] doppler_data_pt_weights        Vector that contains the weights (i.e. 
   *                                              pixel/signal intensities) for each of
   *                                              Doppler-Azimuth image's "edge" points. Each
   *                                              index corresponds to the respective azimuth
   *                                              value.
   */
  void ExtractMeasurementVectors(
    const cv::Mat& doppler_image,
    Eigen::Matrix<double, Eigen::Dynamic, 1>& doppler_data_pt_range_rates,
    Eigen::Matrix<double, Eigen::Dynamic, 1>& doppler_data_pt_weights);

  /**
   * @brief Calculates the Iterative Recursive Least Squares (IRLS) solution of the SONAR's 
   *        translational ego-motion from the Doppler-Azimuth image's "edge" points
   * 
   * @param[in] doppler_data_pt_range_rates         Vector that contains the range rates for each
   *                                                  of Doppler-Azimuth image's "edge" points 
   *                                                  [m/s]. Each index corresponds to the  
   *                                                  respective azimuth value
   * @param[in] doppler_data_pt_weights             Vector that contains the weights (i.e. 
   *                                                  pixel/signal intensities) for each of
   *                                                  Doppler-Azimuth image's "edge" points. Each
   *                                                  index corresponds to the respective azimuth
   *                                                  value.
   * @param[out] translational_velocity_estimate    Two-element vector of the SONAR's translational
   *                                                  ego-motion estimate [v_x v_y] [m/s]
   */
  void IRLSEstimate(
    const Eigen::Matrix<double, Eigen::Dynamic, 1>& doppler_data_pt_range_rates,
    const Eigen::Matrix<double, Eigen::Dynamic, 1>& doppler_data_pt_weights,
    Eigen::Vector2d& translational_velocity_estimate);

  /**
   * @brief Calculates and returns the scaling factor for the SONAR's translational ego-motion
   *        estimate based on depression angles
   * 
   * @param[in] min_mag_return_depress_angle_wrt_vehicle 
   *                                      Minimum (in terms of magnitude) depression angle [rad]
   *                                        of beamwidth's edge with respect to the vehicle's
   *                                        coordinate system
   * @param[in] max_mag_return_depress_angle_wrt_vehicle 
   *                                       Maximum (in terms of magnitude) depression angle [rad]
   *                                        of beamwidth's edge with respect to the vehicle's
   *                                        coordinate system
   * @param[in] a_priori_vehicle_trans_vel_depression_angle 
   *                                       Depression angle [rad] of the a priori estimate of the
   *                                         vehicle's translational velocity with respect to the
   *                                         vehicle's coordinate system
   * @return double                        SONAR's translational ego-motion scaling factor
   */
  double TransVelScalingFactor(
    const double min_mag_return_depress_angle_wrt_vehicle,
    const double max_mag_return_depress_angle_wrt_vehicle,
    const double a_priori_vehicle_trans_vel_depression_angle);

 private:
  const double minimum_vehicle_speed_for_calc_;     // Minimum vehicle speed for a valid
                                                    //   ego-motion estimate calculation
  const uint irls_iterations_;                      // Number of Iterative Recursive Least Squares
                                                    //   (IRLS) iterations
  bool initialized_;                                // Boolean flag indicating if object was
                                                    //   initialized
  cv::Mat filtered_derivative_kernel_;              // Kernel used to simultaneously filter and
                                                    //   take the derivative of the Doppler-Azimuth
                                                    //   image (derivative of Gaussian)
  Eigen::Matrix<double, Eigen::Dynamic, 1> range_rates_;
                                                    // Vector that contains the range rates that
                                                    //   correspond to the rows of the Doppler-
                                                    //   Azimuth image (indexes are the same as
                                                    //   v- image coordinates)
  Eigen::Matrix<double, Eigen::Dynamic, 2> neg_radial_axis_unit_vectors_;
                                                    // Nx2 matrix of negative unit vectors of the
                                                    //   radial axes for each azimuth in the
                                                    //   Doppler- Azimuth image (indexes are
                                                    //   the same as u- image coordinates).
                                                    //   In other words, equal to
                                                    //   [-cos(theta) -sin(theta)] for N azimuths
  double drdot_dv_;                                 // Partial derivative of the range rate with
                                                    //   respect to the Doppler-Azimuth image's
                                                    //   v- image coordinates
  double sonar_depression_angle_rad_wrt_vehicle_;   // SONAR's depression angle [rad] with respect
                                                    //   the vehicle's coordinate system
};
