/********************************COPYRIGHT CHRIS D. MONACO, 2019**********************************/
#include <cmath>
#include <algorithm>
#include <thread>

#include "opencv2/imgproc/imgproc.hpp"

#include "sonarodo/rot_vel_estimator.h"

RotVelEstimator::RotVelEstimator()
    : initialized_(false) { }

void RotVelEstimator::Initialize(const cv::Mat& current_polar_image,
                                 const PolarImageParameters& polar_image_params,
                                 const CartesianImageParameters& cartesian_image_params) {
  polar_image_parameters_ = polar_image_params;
  cartesian_image_parameters_ = cartesian_image_params;

  // Generate images that contain real-world unit values for each polar image pixel
  GenerateUnitValueImages(polar_image_params,
                          polar_azimuth_values_image_,
                          polar_range_values_image_);

  // Generate images that contain the cosine and sine of each polar image pixel's azimuth
  GenerateCosSinAzimuthImages(polar_azimuth_values_image_.row(0),
                              polar_image_params.height,
                              polar_cos_azimuth_values_image_,
                              polar_sin_azimuth_values_image_);

  // Calculate and store optimal DFT size for the polar image
  optimal_dft_image_size_ = cv::Size(cv::getOptimalDFTSize(polar_image_params.width),
                                     cv::getOptimalDFTSize(polar_image_params.height));

  // Create Hann window to attentuates the high frequency components of a
  // cross-power spectrum image. Must be shifted since the low-frequency components of the later
  // cross-power spectrum image is at the top-left corner.
  cv::createHanningWindow(high_frequency_attenuation_window_, optimal_dft_image_size_, CV_64FC1);
  FFTShift(high_frequency_attenuation_window_);

  // Stores current image for next iteration
  StoreImage(current_polar_image);

  initialized_ = true;
}

template<typename SrcCoordSysParams, typename DstCoordSysParams>
void RotVelEstimator::GenerateMap(const SrcCoordSysParams& src_image_params,
                                  const DstCoordSysParams& dst_image_params,
                                  cv::Mat& src2dst_u_map,
                                  cv::Mat& src2dst_v_map) {
  // Calculate images that correspond to the real-world unit values of the destination image
  cv::Mat dst_u_unit_values_image;
  cv::Mat dst_v_unit_values_image;
  GenerateUnitValueImages(dst_image_params,
                          dst_u_unit_values_image,
                          dst_v_unit_values_image);

  // Calculate pixel maps used to remap images
  CalcPixelConversionMap(dst_u_unit_values_image,
                         dst_v_unit_values_image,
                         src_image_params,
                         src2dst_u_map,
                         src2dst_v_map);
}

void RotVelEstimator::GenerateUnitValueImages(const SpatialImageParameters& image_params,
                                              cv::Mat& u_unit_values_image,
                                              cv::Mat& v_unit_values_image) {
  // Generate vectors of real-world unit vectors that correspond to image's u- and v- axes
  Eigen::Array<float, Eigen::Dynamic, 1> u_unit_values_vector;
  Eigen::Array<float, Eigen::Dynamic, 1> v_unit_values_vector;
  GenerateUnitValuesVectors(image_params, u_unit_values_vector, v_unit_values_vector);

  // Generate image versions of the aforementioned vectors
  cv::Mat u_unit_values_image_vector;
  cv::Mat v_unit_values_image_vector;
  GenerateUnitValuesImageVectors(u_unit_values_vector,
                                 v_unit_values_vector,
                                 u_unit_values_image_vector,
                                 v_unit_values_image_vector);

  // Repeat image vectors to make an entire image that corresponds to each image pixel
  u_unit_values_image = cv::repeat(u_unit_values_image_vector,
                                   image_params.height,
                                   1);

  v_unit_values_image = cv::repeat(v_unit_values_image_vector,
                                   1,
                                   image_params.width);
}

void RotVelEstimator::GenerateUnitValuesVectors(
    const SpatialImageParameters& image_params,
    Eigen::Array<float, Eigen::Dynamic, 1>& u_unit_values_vector,
    Eigen::Array<float, Eigen::Dynamic, 1>& v_unit_values_vector) {
  // Generate vectors of real-world unit vectors that correspond to image's u- and v- axes
  // via linearly-spaced values between the first and last pixels
  u_unit_values_vector = Eigen::Array<float, Eigen::Dynamic, 1>::LinSpaced(
    image_params.width,
    - image_params.u0 * image_params.dunit_du,
    ((image_params.width - 1) - image_params.u0) * image_params.dunit_du);

  v_unit_values_vector = Eigen::Array<float, Eigen::Dynamic, 1>::LinSpaced(
    image_params.height,
    - image_params.v0 * image_params.dunit_dv,
    ((image_params.height - 1) - image_params.v0) * image_params.dunit_dv);
}

void RotVelEstimator::GenerateUnitValuesImageVectors(
    Eigen::Array<float, Eigen::Dynamic, 1>& u_unit_values_vector,
    Eigen::Array<float, Eigen::Dynamic, 1>& v_unit_values_vector,
    cv::Mat& u_unit_values_image_vector,
    cv::Mat& v_unit_values_image_vector) {
  // Convert real-world unit vectors into equivalent image vectors
  u_unit_values_image_vector = cv::Mat(1,
                                       u_unit_values_vector.size(),
                                       CV_32FC1,
                                       u_unit_values_vector.data());

  v_unit_values_image_vector = cv::Mat(v_unit_values_vector.size(),
                                       1,
                                       CV_32FC1,
                                       v_unit_values_vector.data());
}

void RotVelEstimator::CalcPixelConversionMap(const cv::Mat& dst_cart_y_values_image,
                                             const cv::Mat& dst_cart_x_values_image,
                                             const PolarImageParameters& src_polar_image_params,
                                             cv::Mat& polar2cart_u_map,
                                             cv::Mat& polar2cart_v_map) {
  // Converts the destination's cartesian x and y values into their equivalent range and azimuths
  cv::Mat equivalent_range_values;
  cv::Mat equivalent_azimuth_rad_values;
  cv::cartToPolar(dst_cart_x_values_image,
                  dst_cart_y_values_image,
                  equivalent_range_values,
                  equivalent_azimuth_rad_values);

  // Ensure azimuthal angles are within the proper range to describe azimuthal deviations
  // from the longitudinal axis
  cv::subtract(equivalent_azimuth_rad_values, 2.0 * M_PI,
               equivalent_azimuth_rad_values,
               equivalent_azimuth_rad_values > M_PI);

  // Create u- and v- maps for image remapping. It is the size of the destination (cartesian)
  // image, but describes the corresponding pixel locations in the source (polar) image
  polar2cart_u_map = equivalent_azimuth_rad_values / src_polar_image_params.dunit_du
                     + src_polar_image_params.u0;
  polar2cart_v_map = equivalent_range_values / src_polar_image_params.dunit_dv
                     + src_polar_image_params.v0;
}

void RotVelEstimator::CalcPixelConversionMap(const cv::Mat& dst_azimuth_rad_values_image,
                                             const cv::Mat& dst_range_values_image,
                                             const CartesianImageParameters& src_cart_image_params,
                                             cv::Mat& cart2polar_u_map,
                                             cv::Mat& cart2polar_v_map) {
  // Converts the destination's polar range and azimuth values into their equivalent cartesian
  // x and y values
  cv::Mat equivalent_x_values;
  cv::Mat equivalent_y_values;
  cv::polarToCart(dst_range_values_image,
                  dst_azimuth_rad_values_image,
                  equivalent_x_values,
                  equivalent_y_values);

  // Create u- and v- maps for image remapping. It is the size of the destination (polar)
  // image, but describes the corresponding pixel locations in the source (cartesian) image
  cart2polar_u_map = equivalent_y_values / src_cart_image_params.dunit_du
                     + src_cart_image_params.u0;
  cart2polar_v_map = equivalent_x_values / src_cart_image_params.dunit_dv
                    + src_cart_image_params.v0;
}

template<>
void RotVelEstimator::Remap<PolarImageParameters, CartesianImageParameters>(
    const cv::Mat& src_polar_image, cv::Mat& dst_cart_image) {
  // If pixel maps for image remapping have not yet been generated, generate them
  if (polar2cart_u_map_.empty() || polar2cart_v_map_.empty()) {
    GenerateMap(polar_image_parameters_, cartesian_image_parameters_,
                polar2cart_u_map_, polar2cart_v_map_);
  }

  // Remaps source polar image into its equivalent destination cartesian image
  cv::remap(src_polar_image,
            dst_cart_image,
            polar2cart_u_map_,
            polar2cart_v_map_,
            CV_INTER_LINEAR);
}

template<>
void RotVelEstimator::Remap<CartesianImageParameters, PolarImageParameters>(
    const cv::Mat& src_cart_image, cv::Mat& dst_polar_image) {
  // If pixel maps for image remapping have not yet been generated, generate them
  if (cart2polar_u_map_.empty() || cart2polar_v_map_.empty()) {
    GenerateMap(cartesian_image_parameters_, polar_image_parameters_,
                cart2polar_u_map_, cart2polar_v_map_);
  }

  // Remaps source cartesian image into its equivalent destination polar image
  cv::remap(src_cart_image,
            dst_polar_image,
            cart2polar_u_map_,
            cart2polar_v_map_,
            CV_INTER_LINEAR);
}

void RotVelEstimator::GenerateCosSinAzimuthImages(
    const cv::Mat& azimuth_values_image_vector,
    const uint polar_image_height,
    cv::Mat& polar_cos_azimuth_values_image,
    cv::Mat& polar_sin_azimuth_values_image) {
  cv::Mat cos_azimuth_values_image_vector =
    cv::Mat(azimuth_values_image_vector.size(), azimuth_values_image_vector.type());
  cv::Mat sin_azimuth_values_image_vector =
    cv::Mat(azimuth_values_image_vector.size(), azimuth_values_image_vector.type());

  // For each pixel in azimuth values image, calculate its cosine and sine value and
  // place it in the corresponding image vector. Iterates over pixels in parallel.
  azimuth_values_image_vector.forEach<float>(
    [&](const float azimuth_value, const int position[]) -> void {
      cos_azimuth_values_image_vector.at<float>(position[1]) = std::cos(azimuth_value);
      sin_azimuth_values_image_vector.at<float>(position[1]) = std::sin(azimuth_value);
    });

  // Repeat cosine and sine image vectors over all ranges to create their entire images
  polar_cos_azimuth_values_image = cv::repeat(cos_azimuth_values_image_vector,
                                              polar_image_height,
                                              1);

  polar_sin_azimuth_values_image = cv::repeat(sin_azimuth_values_image_vector,
                                              polar_image_height,
                                              1);
}

double RotVelEstimator::Estimate(const cv::Mat& current_polar_image,
                                const Eigen::Vector2d& translational_velocity_estimate,
                                const double delta_t,
                                const uint min_detectable_range_image_v) {
  // Remap previous polar image according to the current SONAR's translational ego-motion
  // estimate
  cv::Mat remapped_previous_polar_image;
  RemapPreviousPolarImage(previous_polar_image_,
                          translational_velocity_estimate,
                          delta_t,
                          polar_image_parameters_,
                          polar_range_values_image_,
                          polar_azimuth_values_image_,
                          polar_cos_azimuth_values_image_,
                          polar_sin_azimuth_values_image_,
                          remapped_previous_polar_image);

  // Find azimuth change of the environment from consecutive polar images that corresponds to the
  // SONAR's yaw rate
  const double azimuth_change = FindAzimuthChange(remapped_previous_polar_image,
                                                  current_polar_image,
                                                  polar_image_parameters_,
                                                 min_detectable_range_image_v);

  // Store current polar image for the next iteration
  StoreImage(current_polar_image);

  // The SONAR's yaw rate is equal but opposite to the observed azimuth change over time
  return - azimuth_change / delta_t;
}

void RotVelEstimator::RemapPreviousPolarImage(
    const cv::Mat& previous_polar_image,
    const Eigen::Vector2d& translational_velocity_estimate,
    const double delta_t,
    const PolarImageParameters& polar_image_params,
    const cv::Mat& polar_range_values_image,
    const cv::Mat& polar_azimuth_values_image,
    const cv::Mat& polar_cos_azimuth_values_image,
    const cv::Mat& polar_sin_azimuth_values_image,
    cv::Mat& remapped_previous_polar_image) {
  // Calculate how each environmental point's range should have changed between images based on
  // the current translational ego-motion estimate
  cv::Mat prev2curr_delta_ranges_image =
    - polar_cos_azimuth_values_image * translational_velocity_estimate.x()
    - polar_sin_azimuth_values_image * translational_velocity_estimate.y();
  prev2curr_delta_ranges_image *= delta_t;

  // Calculate how each environmental point's azimuth should have changed between images based on
  //  the current translational ego-motion estimate
  cv::Mat prev2curr_prerot_delta_azimuths_image =
    (polar_sin_azimuth_values_image / polar_range_values_image) *
       translational_velocity_estimate.x()
     - (polar_cos_azimuth_values_image / polar_range_values_image) *
         translational_velocity_estimate.y();
  prev2curr_prerot_delta_azimuths_image *= delta_t;

  // Calculate what the ranges and azimuths of environmental point's should have been in the
  // previous polar image
  cv::Mat corresponding_prev_ranges_image =
    polar_range_values_image - prev2curr_delta_ranges_image;
  cv::Mat corresponding_prev_azimuths_image =
    polar_azimuth_values_image - prev2curr_prerot_delta_azimuths_image;

  // Based on predictions of points' ranges and azimuths in the previous polar image, calculate
  // the corresponding polar image pixel coordinates
  cv::Mat corresponding_prev_u_coords =
    corresponding_prev_azimuths_image / polar_image_params.dunit_du +
    polar_image_params.u0;
  cv::Mat corresponding_prev_v_coords =
    corresponding_prev_ranges_image / polar_image_params.dunit_dv +
    polar_image_params.v0;

  // Remap the previous polar image according to the current translational ego-motion estimate
  cv::remap(previous_polar_image,
            remapped_previous_polar_image,
            corresponding_prev_u_coords,
            corresponding_prev_v_coords,
            CV_INTER_LINEAR);
}

double RotVelEstimator::FindAzimuthChange(const cv::Mat& remapped_previous_polar_image,
                                          const cv::Mat& current_polar_image,
                                          const PolarImageParameters& polar_image_params,
                                          const uint min_detectable_range_image_v) {
  // Set region of interest to all pixels with ranges greater than the minimum detectable range
  cv::Rect region_of_interest(0,
                              0,
                              polar_image_params.width,
                              min_detectable_range_image_v + 1);

  // Deploy the Discrete Fourier Transform (DFT) on the current and remapped previous polar images
  // to decompose them into their frequency components
  cv::Mat previous_remapped_polar_frequency_image;
  cv::Mat current_polar_frequency_image;
  GenerateDFTImages(remapped_previous_polar_image,
                    current_polar_image,
                    region_of_interest,
                    previous_remapped_polar_frequency_image,
                    current_polar_frequency_image);

  // Calculate the image's cross-power spectrum and then deploy the Inverse Discrete Fourier
  // Transform (IDFT) to calculate the phase correlation matrix (but only the first row since
  // it corresponds to a zero range offset)
  cv::Mat idft_image_row;
  GenerateIDFTImage(previous_remapped_polar_frequency_image,
                    current_polar_frequency_image,
                    region_of_interest.height,
                    idft_image_row);

  // Find the maximum of the phase correlation matrix row and then refine it to sub-pixel
  // resolution
  cv::Point2i idft_row_max_loc;
  cv::minMaxLoc(idft_image_row, nullptr, nullptr, nullptr, &idft_row_max_loc);
  cv::Point2d refined_idft_row_max_loc = idft_row_max_loc;
  RefinePeakLocation(idft_image_row, refined_idft_row_max_loc);

  // Since the image is cyclic, convert large u- coordinate offsets to negative
  // u- coordinate offsets
  const double u_coordinate_offset =
    (refined_idft_row_max_loc.x < (idft_image_row.cols / 2)) ?
    refined_idft_row_max_loc.x :
    refined_idft_row_max_loc.x - static_cast<double>(idft_image_row.cols);

  // Return the azimuthal offset that corresponds to the u coordinate offset
  return u_coordinate_offset * polar_image_params.dunit_du;
}

void RotVelEstimator::GenerateDFTImages(
    const cv::Mat& previous_remapped_polar_spatial_image,
    const cv::Mat& current_polar_spatial_image,
    const cv::Rect& region_of_interest,
    cv::Mat& previous_remapped_polar_frequency_image,
    cv::Mat& current_polar_frequency_image) {
  // Create a Hann window filter the size of the region of interest
  cv::Mat hanning_window;
  cv::createHanningWindow(hanning_window, region_of_interest.size(), CV_64FC1);

  // In two separate threads (one for the previous image and one for the current image),
  // "prep" the spatial polar images (pad them to the optimal size and filter them with the
  // Hann window). Then, decompose them into their frequency components via the Discrete
  // Fourier Transform (DFT). Processes run in parallel

  std::thread thread_for_previous_image([&]() {
  cv::Mat prepped_previous_remapped_polar_spatial_image;
  GeneratePreppedSpatialImage(previous_remapped_polar_spatial_image,
                              region_of_interest,
                              hanning_window,
                              prepped_previous_remapped_polar_spatial_image);
  cv::dft(prepped_previous_remapped_polar_spatial_image,
          previous_remapped_polar_frequency_image,
          0,
          region_of_interest.height);
  });

  std::thread thread_for_current_image([&]() {
  cv::Mat prepped_current_polar_spatial_image;
  GeneratePreppedSpatialImage(current_polar_spatial_image,
                              region_of_interest,
                              hanning_window,
                              prepped_current_polar_spatial_image);
  cv::dft(prepped_current_polar_spatial_image,
          current_polar_frequency_image,
          0,
          region_of_interest.height);
  });

  // Wait until both threads have finished processing
  thread_for_previous_image.join();
  thread_for_current_image.join();
}

void RotVelEstimator:: GeneratePreppedSpatialImage(const cv::Mat& polar_spatial_image,
                                                   const cv::Rect& region_of_interest,
                                                   const cv::Mat& hanning_window,
                                                   cv::Mat& prepped_polar_spatial_image) {
  // Pad image with zeros to make it the optimal DFT size
  cv::Mat padded_polar_spatial_image;
  cv::copyMakeBorder(polar_spatial_image,
                     padded_polar_spatial_image,
                     0,
                     optimal_dft_image_size_.height - polar_spatial_image.rows,
                     0,
                     optimal_dft_image_size_.width - polar_spatial_image.cols,
                     cv::BORDER_CONSTANT,
                     cv::Scalar::all(0));

  // Convert ushort image to a floating point image and scale everything to the range of 0 - 1
  padded_polar_spatial_image.convertTo(
    padded_polar_spatial_image,
    CV_64FC1,
    1.0 / static_cast<double>(std::numeric_limits<ushort>::max()));

  // Create two images that correspond to the real and imaginary components of the spatial polar
  // image, respectively
  cv::Mat prepped_polar_spatial_image_channels[2];
  prepped_polar_spatial_image_channels[0] = cv::Mat::zeros(optimal_dft_image_size_, CV_64FC1);
  prepped_polar_spatial_image_channels[1] = cv::Mat::zeros(optimal_dft_image_size_, CV_64FC1);

  // Populate the real component of the image only in the region of interest. Also, only populate
  // it with the padded polar spatial image after being filtered by the Hann window.
  prepped_polar_spatial_image_channels[0](region_of_interest) =
    padded_polar_spatial_image(region_of_interest).mul(hanning_window);

  // Merge the two real and imaginary component images into one image with two channels
  cv::merge(prepped_polar_spatial_image_channels,
            2,
            prepped_polar_spatial_image);
}

void RotVelEstimator::GenerateIDFTImage(const cv::Mat& previous_remapped_polar_frequency_image,
                                        const cv::Mat& current_polar_frequency_image,
                                        const uint region_of_interest_height,
                                        cv::Mat& idft_image_row) {
  // Calculate the cross power spectrum of the current and previous remapped polar images.
  // Note that the flags in this function take the complex conjugate of the second input array
  // (in this case, the current polar image)
  cv::Mat cross_power_spectrum;
  cv::mulSpectrums(previous_remapped_polar_frequency_image,
                   current_polar_frequency_image,
                   cross_power_spectrum,
                   0,
                   true);

  // Extract the two cross power spectrum channels into two separate images. They correspond to
  // the real and imaginary components, respectively
  cv::Mat cross_power_spectrum_channels[2];
  cv::extractChannel(cross_power_spectrum, cross_power_spectrum_channels[0], 0);
  cv::extractChannel(cross_power_spectrum, cross_power_spectrum_channels[1], 1);

  // Calculate the magnitude for each pixel of the cross power spectrum
  cv::Mat cross_power_spectrum_magnitude;
  cv::magnitude(cross_power_spectrum_channels[0],
                cross_power_spectrum_channels[1],
                cross_power_spectrum_magnitude);

  // Normalize the cross power spectrum channels by their pixels' magnitudes
  cross_power_spectrum_channels[0] /= cross_power_spectrum_magnitude;
  cross_power_spectrum_channels[1] /= cross_power_spectrum_magnitude;

  // Attentuate the high frequencies in the cross power spectrum by multiplying them
  // with the high frequency attentuation window
  cross_power_spectrum_channels[0] =
    cross_power_spectrum_channels[0].mul(high_frequency_attenuation_window_);
  cross_power_spectrum_channels[1] =
    cross_power_spectrum_channels[1].mul(high_frequency_attenuation_window_);

  // Merge the two separate images into two channels of one cross_power_spectrum image
  cv::merge(cross_power_spectrum_channels, 2, cross_power_spectrum);

  // Deploy the Inverse Discrete Fourier Transform (IDFT) on the cross_power_spectrum image
  // to calculate the phase correlation matrix
  cv::Mat idft_image;
  cv::idft(cross_power_spectrum, idft_image, cv::DFT_REAL_OUTPUT, region_of_interest_height);

  // Only output the first row since only this row corresponds to a zero range offset
  idft_image_row = idft_image.row(0);
}

void RotVelEstimator::RefinePeakLocation(const cv::Mat& idft_image_row,
                                         cv::Point2d& peak_point) {
  // Create a three-element vector of the peak_point and its neighbors, wrapping around the
  // image edges if necessary

  Eigen::Array<double, 1, 3> peak_region_values;
  peak_region_values[1] = idft_image_row.at<double>(peak_point);

  if (peak_point.x == 0) {
    peak_region_values[0] = idft_image_row.at<double>(0, idft_image_row.cols - 1);
    peak_region_values[2] = idft_image_row.at<double>(0, peak_point.x + 1);
  } else if (peak_point.x == (idft_image_row.cols - 1)) {
    peak_region_values[0] = idft_image_row.at<double>(0, peak_point.x - 1);
    peak_region_values[2] = idft_image_row.at<double>(0, 0);
  } else {
    peak_region_values[0] = idft_image_row.at<double>(0, peak_point.x - 1);
    peak_region_values[2] = idft_image_row.at<double>(0, peak_point.x + 1);
  }

  // Calculate the weighted centroid of the peak point within its its three-element neighborhood
  // Represents pixel deviation from the middle element (that corresponds to the original peak)
  const double peak_point_delta_u =
    (peak_region_values[2] - peak_region_values[0]) / peak_region_values.sum();

  // Add the local weighted centroid value to the peak point's value to output a peak with
  // sub-pixel resolution, but do not allow the weighted centroid to change the peak's pixel
  // coordinate by more than +/- 1 pixel
  peak_point.x += std::min(std::max(peak_point_delta_u, -1.0), +1.0);
}

void RotVelEstimator::FFTShift(cv::Mat& image) {
  // Calculate image pixel centers, rounding down to nearest integer if necessary
  const int cx = image.cols/2;
  const int cy = image.rows/2;

  // Create four quadrant images that represent all four quadrants of the image.
  // 0 = top-left; 1 = top-right; 2 = bottom-left, 3 = bottom-right
  cv::Mat quadrant_images[4];
  quadrant_images[0] = image(cv::Rect(0, 0, cx, cy));
  quadrant_images[1] = image(cv::Rect(cx, 0, image.cols - cx, cy));
  quadrant_images[2] = image(cv::Rect(0, cy, cx, image.rows - cy));
  quadrant_images[3] = image(cv::Rect(cx, cy, image.cols - cx, image.rows - cy));

  // Two operations of swapping quadrants. Creates a temporary quadrant to temporarily store
  // data that is rewritten in the original image.
  for (uint idx = 0; idx < 2; ++idx) {
    cv::Mat temporary_quadrant_image;
    quadrant_images[idx].copyTo(temporary_quadrant_image);
    quadrant_images[4 - 1 - idx].copyTo(quadrant_images[idx]);
    temporary_quadrant_image.copyTo(quadrant_images[4 - 1 - idx]);
  }
}
