/********************************COPYRIGHT CHRIS D. MONACO, 2019**********************************/
#include <limits>
#include <cmath>

#include "cv_bridge/cv_bridge.h"
#include "opencv2/core/core.hpp"
#include "eigen3/Eigen/Core"
#include "eigen3/Eigen/Geometry"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "geometry_msgs/TwistWithCovarianceStamped.h"

#include "sonarodo/sonarodo.h"

static constexpr double kMinimumVehicleSpeedForCalc = 0.5;     // Minimum vehicle speed for valid
                                                               //   ego-motion estimate calc.
static constexpr uint kFilteredDerivativeKernelSize = 5;       // Pixel size of kernel used to take
                                                               //  derivative of Doppler-Azimuth
                                                               //  image
static constexpr uint kIRLSIterations = 5;                     // Number of iterations for IRLS
                                                               //   solution for trans. vel.
static constexpr double kPolarImageHeightScaleFactor = 1.0;    // Scale factor for resized polar
                                                               //   image's height
static constexpr double kPolarImageWidthScaleFactor = 1.0;     // Scale factor for resized polar
                                                               //   image's width
static constexpr double kCartesianImageAspectRatio = 1.85;     // Aspect ratio of equivalent
                                                               //   Cartesian image

Sonarodo::Sonarodo()
    : polar_image_height_scale_factor_(kPolarImageHeightScaleFactor),
      polar_image_width_scale_factor_(kPolarImageWidthScaleFactor),
      cartesian_image_aspect_ratio_(kCartesianImageAspectRatio),
      initialized_(false),
      trans_vel_estimator_(kMinimumVehicleSpeedForCalc,
                           kFilteredDerivativeKernelSize,
                           kIRLSIterations) {
  sonar_data_subscriber_ = nh_.subscribe(
    "/sonar",
    2,
    &Sonarodo::SonarDataCallback,
    this);

  sonar_egomotion_publisher_ =
    nh_.advertise<geometry_msgs::TwistWithCovarianceStamped>("/sonar_egomotion", 2);
}

void Sonarodo::SonarDataCallback(const sonar_msgs::SonarConstPtr sonar_msg_ptr) {
  // Convert Range-Azimuth (polar) ROS message image to OpenCV image
  cv::Mat polar_image = cv_bridge::toCvShare(sonar_msg_ptr->polar_image, sonar_msg_ptr)->image;

  cv::Mat resized_polar_image;
  ResizeImage(polar_image,
              polar_image_width_scale_factor_,
              polar_image_height_scale_factor_,
              resized_polar_image);

  if (!initialized_) {
    Initialize(*sonar_msg_ptr, resized_polar_image, cartesian_image_aspect_ratio_);
    return;
  }

  Eigen::Vector3d euler_angles_rad;
  ExtractEulerAngles(sonar_msg_ptr->pose.orientation, euler_angles_rad);
  Eigen::Vector3d a_priori_vehicle_trans_vel_estimate(sonar_msg_ptr->twist.linear.x,
                                                      sonar_msg_ptr->twist.linear.y,
                                                      sonar_msg_ptr->twist.linear.z);

  // Convert ROS Doppler-Azimuth image message to OpenCV image
  cv::Mat doppler_image = cv_bridge::toCvShare(sonar_msg_ptr->doppler_image, sonar_msg_ptr)->image;

  // Find minimum and maximum detectable ranges from Range-Azimuth image
  double min_detectable_range, max_detectable_range;
  const uint min_detectable_range_image_v = FindDetectableRanges(polar_image_params_,
                                                                 resized_polar_image,
                                                                 min_detectable_range,
                                                                 max_detectable_range);

  // Estimates translational ego-motion largely from the Doppler-Azimuth image.
  // Only continue if estimate is valid, otherwise prepare for next iteration
  Eigen::Vector2d translational_velocity_estimate;
  if (!trans_vel_estimator_.Estimate(doppler_image,
                                     min_detectable_range,
                                     max_detectable_range,
                                     sonar_msg_ptr->bottom_sounder_depth_m,
                                     euler_angles_rad.y(),
                                     a_priori_vehicle_trans_vel_estimate,
                                     translational_velocity_estimate)) {
    rot_vel_estimator_.StoreImage(resized_polar_image);
    previous_msg_timestamp_ = sonar_msg_ptr->header.stamp;
    return;
  }

  // Estimate yaw rate from consecutive Range-Azimuth images
  const double delta_t = (sonar_msg_ptr->header.stamp - previous_msg_timestamp_).toSec();
  const double yaw_rate_estimate =
    rot_vel_estimator_.Estimate(resized_polar_image,
                                translational_velocity_estimate,
                                delta_t,
                                min_detectable_range_image_v);

  // Publish the ego-motion estimate over ROS
  PublishEgomotionEstimate(translational_velocity_estimate,
                           yaw_rate_estimate,
                           sonar_msg_ptr->header);

  previous_msg_timestamp_ = sonar_msg_ptr->header.stamp;
}

void Sonarodo::Initialize(const sonar_msgs::Sonar& sonar_msg,
                          const cv::Mat& polar_image,
                          const double cartesian_image_aspect_ratio) {
  // If the translational velocity estimator is not initialized, initialize it
  if (!trans_vel_estimator_.is_initialized()) {
    trans_vel_estimator_.Initialize(sonar_msg.doppler_image.width,
                                    sonar_msg.doppler_image.height,
                                    sonar_msg.min_azimuth_deg * M_PI / 180.0,
                                    sonar_msg.max_azimuth_deg * M_PI / 180.0,
                                    sonar_msg.min_range_rate_mps,
                                    sonar_msg.max_range_rate_mps,
                                    sonar_msg.sonar_depression_deg * M_PI / 180.0);
  }

  // If the rotational velocity estimator is not initialized, initialize it
  if (!rot_vel_estimator_.is_initialized()) {
    const double true_image_width_resizing_scale =
      static_cast<double>(polar_image.cols) / sonar_msg.polar_image.width;
    const double true_image_height_resizing_scale =
      static_cast<double>(polar_image.rows) / sonar_msg.polar_image.height;

    polar_image_params_ = PolarImageParameters(
      polar_image.cols,
      polar_image.rows,
      sonar_msg.polar_image_u0 * true_image_width_resizing_scale,
      sonar_msg.polar_image_v0 * true_image_height_resizing_scale,
      (sonar_msg.max_azimuth_deg - sonar_msg.min_azimuth_deg) * (M_PI / 180.0) /
        static_cast<double>(polar_image.cols),
      (sonar_msg.min_range_m - sonar_msg.max_range_m) /
        static_cast<double>(polar_image.rows));

    const CartesianImageParameters cartesian_image_params(
                     polar_image_params_.height * cartesian_image_aspect_ratio,
                     polar_image_params_.height,
                     polar_image_params_.height * cartesian_image_aspect_ratio * 0.5,
                     polar_image_params_.v0,
                     std::abs(polar_image_params_.dunit_dv),
                     - std::abs(polar_image_params_.dunit_dv));

    rot_vel_estimator_.Initialize(polar_image,
                                  polar_image_params_,
                                  cartesian_image_params);
  }

  previous_msg_timestamp_ = sonar_msg.header.stamp;

  initialized_ = trans_vel_estimator_.is_initialized() &&
                 rot_vel_estimator_.is_initialized();
}

void Sonarodo::ResizeImage(const cv::Mat& original_image,
                           const double width_scale_factor,
                           const double height_scale_factor,
                           cv::Mat& resized_image) {
  // Resize image, but only if necessary
  if (width_scale_factor == 1 && height_scale_factor == 1) {
    resized_image = original_image;
  } else {
    cv::resize(original_image, resized_image,
               cv::Size(), width_scale_factor, height_scale_factor);
  }
}

void Sonarodo::PublishEgomotionEstimate(
    const Eigen::Vector2d& translational_velocity_estimate,
    const double yaw_rate_estimate,
    const std_msgs::Header& msg_header) {

  geometry_msgs::TwistWithCovarianceStamped sonar_egomotion_msg;
  sonar_egomotion_msg.header = msg_header;

  sonar_egomotion_msg.twist.twist.linear.x = translational_velocity_estimate.x();
  sonar_egomotion_msg.twist.twist.linear.y = translational_velocity_estimate.y();

  sonar_egomotion_msg.twist.twist.angular.z = yaw_rate_estimate;

  sonar_egomotion_publisher_.publish(sonar_egomotion_msg);
}

void Sonarodo::Visualize(const cv::Mat& image,
                         const std::string& image_window_name,
                         const bool convert_to_colormap,
                         const int delay_ms) {
  cv::Mat display_image = image;

  // If image is to be converted to a colormap, it must be normalized based on its datatype
  // and then converted to a uchar image with the correct scaling
  if (convert_to_colormap) {
    double input_image_datatype_max;
    switch (display_image.type()) {
      case CV_8U:
        input_image_datatype_max = std::numeric_limits<uchar>::max();
        break;
      case CV_16U:
        input_image_datatype_max = std::numeric_limits<ushort>::max();
        break;
      default:
        input_image_datatype_max = 1.0;
        break;
    }

    display_image.convertTo(display_image,
                            CV_8U,
                            std::numeric_limits<uchar>::max() / input_image_datatype_max);
    cv::applyColorMap(display_image, display_image, cv::COLORMAP_JET);
  }

  cv::namedWindow(image_window_name);
  cv::imshow(image_window_name, display_image);
  cv::waitKey(delay_ms);
}

uint Sonarodo::FindDetectableRanges(const PolarImageParameters& polar_image_params,
                                    const cv::Mat& polar_image,
                                    double& min_detectable_range,
                                    double& max_detectable_range) {
  // Extract the Range-Azimuth image column corresponding to a zero azimuth
  const cv::Mat zero_azimuth_range_image = polar_image.col(std::round(polar_image_params.u0));

  // Find the maximum detctable range by starting from the maximum range (first row) and
  // continually iterating downwards until the signal/pixel intensity is greater than zero.
  int max_detectable_range_row_idx = 0;
  max_detectable_range = polar_image_params.GetUnitValue(max_detectable_range_row_idx, 1);
  for (int row_idx = 0; row_idx < polar_image.rows; ++row_idx) {
    if (zero_azimuth_range_image.at<ushort>(row_idx)) {
      max_detectable_range = polar_image_params.GetUnitValue(row_idx, 1);
      max_detectable_range_row_idx = row_idx;
      break;
    }
  }

  // Find the minimum detctable range by starting from the minimum range (last row) and
  // continually iterating upwards until the signal/pixel intensity is greater than zero.
  uint min_detectable_range_image_v = polar_image.rows - 1;
  min_detectable_range = polar_image_params.GetUnitValue(min_detectable_range_image_v, 1);
  for (int row_idx = polar_image.rows - 1; row_idx >= max_detectable_range_row_idx; --row_idx) {
    if (zero_azimuth_range_image.at<ushort>(row_idx)) {
      min_detectable_range_image_v = row_idx;
      min_detectable_range = polar_image_params.GetUnitValue(row_idx, 1);
      break;
    }
  }

  return min_detectable_range_image_v;
}

void Sonarodo::ExtractEulerAngles(const geometry_msgs::Quaternion& _quaternion,
                                  Eigen::Vector3d& euler_angles_rad) {
  // Converts the quaternion to their equivalent Euler angles based on the ZYX order but then
  // reports it in a XYZ order
  Eigen::Quaterniond quaternion(_quaternion.w,
                                _quaternion.x,
                                _quaternion.y,
                                std::abs(_quaternion.z));
  euler_angles_rad = quaternion.toRotationMatrix().eulerAngles(2, 1, 0).reverse();
}
