/********************************COPYRIGHT CHRIS D. MONACO, 2019**********************************/
#include <iostream>
#include <vector>
#include <numeric>
#include <algorithm>

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "eigen3/Eigen/Dense"

#include "sonarodo/trans_vel_estimator.h"

TransVelEstimator::TransVelEstimator(const double minimum_vehicle_speed_for_calc,
                                     const uint filtered_derivative_kernel_size,
                                     const uint irls_iterations)
    : minimum_vehicle_speed_for_calc_(minimum_vehicle_speed_for_calc),
      irls_iterations_(irls_iterations),
      initialized_(false) {
  // Ensure kernel size is greater than 3 and odd
  const uint valid_filtered_derivative_kernel_size =
    filtered_derivative_kernel_size > 2 && filtered_derivative_kernel_size % 2 == 1 ?
    filtered_derivative_kernel_size :
    std::max(filtered_derivative_kernel_size + 1, static_cast<uint>(3));
  const uint kernel_center_idx = (valid_filtered_derivative_kernel_size - 1) / 2;

  // Get coeffificents for a Gaussian kernel, reshape it to be a vertical vector. Set middle
  // element to be zero
  filtered_derivative_kernel_ =
    cv::getGaussianKernel(valid_filtered_derivative_kernel_size, -1, CV_64F).reshape(
      1, valid_filtered_derivative_kernel_size);
  filtered_derivative_kernel_.at<double>(kernel_center_idx) = 0;

  // Normalize kernel and set upper elements to their negatives to turn it into a vertical
  // derivative kernel. This emulates the derivative of a Gaussian to simultaneously filter
  // an image while taking its derivative
  filtered_derivative_kernel_ *= 2.0 / cv::sum(filtered_derivative_kernel_)[0];
  for (uint kernel_idx = 0; kernel_idx < kernel_center_idx; ++kernel_idx) {
    filtered_derivative_kernel_.at<double>(kernel_idx) *= -1;
  }
}

void TransVelEstimator::Initialize(const uint doppler_image_width,
                                   const uint doppler_image_height,
                                   const double min_azimuth_rad,
                                   const double max_azimuth_rad,
                                   const double min_range_rate,
                                   const double max_range_rate,
                                   const double sonar_depression_angle_rad_wrt_vehicle) {
  // Calculates range rates corresponding to each Doppler-Azimuth row
  range_rates_ =
    Eigen::Array<double, Eigen::Dynamic, 1>::LinSpaced(
      doppler_image_height, max_range_rate, min_range_rate);

  // Calculates azimuths corresponding to each Doppler-Azimuth column
  Eigen::Array<double, Eigen::Dynamic, 1> azimuth_angles =
    Eigen::Array<double, Eigen::Dynamic, 1>::LinSpaced(
      doppler_image_width, min_azimuth_rad, max_azimuth_rad);

  // Calculate the negative of the unit vectors corresponding to each azimuthal angle
  // in the Doppler-Azimuth image
  neg_radial_axis_unit_vectors_ =
    Eigen::Matrix<double, Eigen::Dynamic, 2>(doppler_image_width, 2);
  neg_radial_axis_unit_vectors_ << - azimuth_angles.cos(), - azimuth_angles.sin();

  // Calculate the partial derivative of the range rate with respect to the Doppler-Azimuth
  // image's v-coordinate
  drdot_dv_ = (min_range_rate - max_range_rate) / static_cast<double>(doppler_image_height);

  sonar_depression_angle_rad_wrt_vehicle_ = sonar_depression_angle_rad_wrt_vehicle;

  initialized_ = true;
}

bool TransVelEstimator::Estimate(const cv::Mat& doppler_image,
                                 const double min_detectable_range,
                                 const double max_detectable_range,
                                 const double echo_sounder_depth,
                                 const double vehicle_pitch,
                                 const Eigen::Vector3d& a_priori_vehicle_trans_vel_estimate,
                                 Eigen::Vector2d& translational_velocity_estimate) {
  // Estimate the beamwidth edges' and the a priori translational velocity vector's
  // depression angles
  double min_mag_return_depress_angle_wrt_vehicle;
  double max_mag_return_depress_angle_wrt_vehicle;
  double a_priori_vehicle_trans_vel_depression_angle;
  EstimateDepressionAngles(min_detectable_range,
                           max_detectable_range,
                           echo_sounder_depth,
                           vehicle_pitch,
                           a_priori_vehicle_trans_vel_estimate,
                           min_mag_return_depress_angle_wrt_vehicle,
                           max_mag_return_depress_angle_wrt_vehicle,
                           a_priori_vehicle_trans_vel_depression_angle);

  // Only continue if a valid estimate is feasible (i.e. if the a priori translational velocity
  // vector resides between the SONAR's beamwidth edges)
  if (!ValidEstimateFeasible(min_mag_return_depress_angle_wrt_vehicle,
                             max_mag_return_depress_angle_wrt_vehicle,
                             a_priori_vehicle_trans_vel_depression_angle)) return false;

  // Extract range rates and weights from points on the Doppler-Azimuth image's "edge"
  // Vector indices correspond to azimuths
  Eigen::Matrix<double, Eigen::Dynamic, 1> doppler_data_pt_range_rates;
  Eigen::Matrix<double, Eigen::Dynamic, 1> doppler_data_pt_weights;
  ExtractMeasurementVectors(doppler_image,
                            doppler_data_pt_range_rates,
                            doppler_data_pt_weights);

  // Based on range rates, weights, and implied azimuths, estimate the translational ego-motion
  // via Iterative Recursive Least Squares (IRLS)
  IRLSEstimate(doppler_data_pt_range_rates,
               doppler_data_pt_weights,
               translational_velocity_estimate);

  // Calculate the scaling factor for the translational ego-motion estimate based on the
  // depression angles. Scale the estimate by this factor.
  translational_velocity_estimate *=
    TransVelScalingFactor(min_mag_return_depress_angle_wrt_vehicle,
                          max_mag_return_depress_angle_wrt_vehicle,
                          a_priori_vehicle_trans_vel_depression_angle);

  return true;
}

void TransVelEstimator::EstimateDepressionAngles(
    const double min_detectable_range,
    const double max_detectable_range,
    const double echo_sounder_depth,
    const double vehicle_pitch,
    const Eigen::Vector3d& a_priori_vehicle_trans_vel_estimate,
    double& min_mag_return_depress_angle_wrt_vehicle,
    double& max_mag_return_depress_angle_wrt_vehicle,
    double& a_priori_vehicle_trans_vel_depression_angle) {
  // Use the law of sines to calculate the two depression angles (with respect to the vehicle's
  // reference frame) based on the echo sounder's depth measurement (aligned with the vehicle's
  // z-axis), and min./max. detectable ranges from the Range-Azimuth image, and the vehicle's
  // pitch
  const double sonar_return_depression_angle_1_wrt_vehicle =
    std::asin((echo_sounder_depth / max_detectable_range) * std::cos(- vehicle_pitch));
  const double sonar_return_depression_angle_2_wrt_vehicle =
    std::asin((echo_sounder_depth / min_detectable_range) * std::cos(- vehicle_pitch));

  // Compare these two depression angles to determine which one is the minimum depression angle
  // (with regards to magnitude) and which one is the maximum
  if (std::abs(sonar_return_depression_angle_1_wrt_vehicle) <
        std::abs(sonar_return_depression_angle_2_wrt_vehicle)) {
    min_mag_return_depress_angle_wrt_vehicle =
      sonar_return_depression_angle_1_wrt_vehicle;
    max_mag_return_depress_angle_wrt_vehicle =
      sonar_return_depression_angle_2_wrt_vehicle;
  } else {
    min_mag_return_depress_angle_wrt_vehicle =
      sonar_return_depression_angle_2_wrt_vehicle;
    max_mag_return_depress_angle_wrt_vehicle =
      sonar_return_depression_angle_1_wrt_vehicle;
  }

  const double a_priori_vehicle_speed_estimate = a_priori_vehicle_trans_vel_estimate.norm();

  // Calculate the vehicle's a priori translational velocity's depression angle. However, this
  // estimate is only valid if the vehicle speed is above the set threshold
  a_priori_vehicle_trans_vel_depression_angle =
    a_priori_vehicle_speed_estimate >= minimum_vehicle_speed_for_calc_ ?
    std::asin(a_priori_vehicle_trans_vel_estimate.z() / a_priori_vehicle_speed_estimate) :
    std::nan("");
}

bool TransVelEstimator::ValidEstimateFeasible(
    const double min_mag_return_depress_angle_wrt_vehicle,
    const double max_mag_return_depress_angle_wrt_vehicle,
    const double a_priori_vehicle_trans_vel_depression_angle) {
  bool valid_estimate_feasible = true;

  // A valid estimte is feasible is the calculated a priori vehicle translational
  // velocitiy's depression angle is finite and if resides between the calculate
  // beamwidth edges (only the beamwidth's depression angles are considered)
  valid_estimate_feasible &= std::isfinite(a_priori_vehicle_trans_vel_depression_angle);
  valid_estimate_feasible &= !IsInRange(min_mag_return_depress_angle_wrt_vehicle,
                                        a_priori_vehicle_trans_vel_depression_angle,
                                        max_mag_return_depress_angle_wrt_vehicle);

  return valid_estimate_feasible;
}

void TransVelEstimator::ExtractMeasurementVectors(
    const cv::Mat& doppler_image,
    Eigen::Matrix<double, Eigen::Dynamic, 1>& doppler_data_pt_range_rates,
    Eigen::Matrix<double, Eigen::Dynamic, 1>& doppler_data_pt_weights) {
  // Filter Doppler-Azimuth image with a kernel that represents the derivative of a Gaussian
  // to take the filtered derivative of the image. Abs value taken so result contains the
  // derivative's magnitude.
  cv::Mat filtered_rdot_derivative_doppler_image;
  cv::filter2D(doppler_image,
               filtered_rdot_derivative_doppler_image,
               CV_64FC1,
               filtered_derivative_kernel_);
  filtered_rdot_derivative_doppler_image = cv::abs(filtered_rdot_derivative_doppler_image);

  // For each azimuth (i.e. image column):
  doppler_data_pt_range_rates = Eigen::Matrix<double, Eigen::Dynamic, 1>(doppler_image.cols);
  doppler_data_pt_weights = Eigen::Matrix<double, Eigen::Dynamic, 1>(doppler_image.cols);
  for (int doppler_image_u = 0; doppler_image_u < doppler_image.cols; ++doppler_image_u) {
    // Extract that image column
    const cv::Rect image_col_rect(doppler_image_u, 0, 1, doppler_image.rows);

    // Find the maximum in that image column. This is the extracted Doppler-Azimuth "edge" point.
    cv::Point2i doppler_edge_point;
    cv::minMaxLoc(filtered_rdot_derivative_doppler_image(image_col_rect),
                  nullptr,
                  nullptr,
                  nullptr,
                  &doppler_edge_point);
    doppler_edge_point.x = doppler_image_u;

    // Extract the weight of that extracted point by extracting its corresponding signal/pixel
    // intensity in the Range-Azimuth image. It is then normalized so that all weights are from
    // 0 - 1. It is added to the weights vector at the index corresponding to its azimuth (or image
    // u-coordinate in the Doppler-Azimuth image)
    doppler_data_pt_weights(doppler_image_u) =
      std::max(1.0, static_cast<double>(doppler_image.at<ushort>(doppler_edge_point))) /
      static_cast<double>(std::numeric_limits<ushort>::max());

    // Add point to the range rates vector as well
    doppler_data_pt_range_rates(doppler_image_u) = range_rates_(doppler_edge_point.y);
  }
}

void TransVelEstimator::IRLSEstimate(
    const Eigen::Matrix<double, Eigen::Dynamic, 1>& doppler_data_pt_range_rates,
    const Eigen::Matrix<double, Eigen::Dynamic, 1>& doppler_data_pt_weights,
    Eigen::Vector2d& translational_velocity_estimate) {
  Eigen::Matrix<double, Eigen::Dynamic, 1> iteration_doppler_data_pt_weights =
    doppler_data_pt_weights;
  Eigen::Matrix<double, Eigen::Dynamic, 1> iteration_unweighted_range_rate_abs_residuals =
    Eigen::Matrix<double, Eigen::Dynamic, 1>(doppler_data_pt_range_rates.rows());

  for (uint irls_iteration = 0; irls_iteration < irls_iterations_; ++irls_iteration) {
    // Calculate the SONAR's translational ego-motion estimate [v_x v_y] by taking the
    // psuedoinverse of the linear system relating azimuths, translational ego-motion,
    // and range rates
    translational_velocity_estimate =
      (neg_radial_axis_unit_vectors_.transpose() *
        iteration_doppler_data_pt_weights.asDiagonal() *
        neg_radial_axis_unit_vectors_).ldlt().solve(
          neg_radial_axis_unit_vectors_.transpose() *
          iteration_doppler_data_pt_weights.asDiagonal() *
          doppler_data_pt_range_rates);

    // Calculate the magnitude of the resulting range rate residuals
    iteration_unweighted_range_rate_abs_residuals =
      (neg_radial_axis_unit_vectors_ * translational_velocity_estimate
      - doppler_data_pt_range_rates).cwiseAbs();

    // Ensure residuals are not below the range rate corresponding to half of a pixel's range
    // rate resolution. If they are, saturate the residuals to this value. This prevents
    // later numerical instabilities (i.e. dividing by zero)
    iteration_unweighted_range_rate_abs_residuals =
      (iteration_unweighted_range_rate_abs_residuals.array() > std::abs(drdot_dv_ / 2.0)).select(
        iteration_unweighted_range_rate_abs_residuals, std::abs(drdot_dv_ / 2.0));

    // Calculate the new weights for each data point based on the previous iteration's weights and
    // current iteration's range rate residuals
    iteration_doppler_data_pt_weights =
      iteration_doppler_data_pt_weights.cwiseQuotient(
        iteration_unweighted_range_rate_abs_residuals);

    // Normalize all weights so they reside in the range from 0 to 1
    iteration_doppler_data_pt_weights /= iteration_doppler_data_pt_weights.maxCoeff();
  }
}

double TransVelEstimator::TransVelScalingFactor(
    const double min_mag_return_depress_angle_wrt_vehicle,
    const double max_mag_return_depress_angle_wrt_vehicle,
    const double a_priori_vehicle_trans_vel_depression_angle) {
  // Set the Doppler-Azimuth image edge's depression angles to whichever of the
  // two beamwidth edge depression angles are closed to the a priori translational velocity
  // vector's depression angle
  const double doppler_azimuth_edge_depress_angle_wrt_vehicle =
    std::abs(min_mag_return_depress_angle_wrt_vehicle
             - a_priori_vehicle_trans_vel_depression_angle) <
      std::abs(max_mag_return_depress_angle_wrt_vehicle
               - a_priori_vehicle_trans_vel_depression_angle) ?
    min_mag_return_depress_angle_wrt_vehicle :
    max_mag_return_depress_angle_wrt_vehicle;

  // Calculate the depression angle of the SONAR with respect to the a priori
  // vehicle translational velocity vector
  const double depress_angle_SONAR_wrt_trans_vel = sonar_depression_angle_rad_wrt_vehicle_
                                                   - a_priori_vehicle_trans_vel_depression_angle;
  // Calculate the depression angle of the Doppler-Azimuth image edge with respect to the
  // a priori vehicle translational velocity vector
  const double depress_angle_edge_wrt_trans_vel = doppler_azimuth_edge_depress_angle_wrt_vehicle
                                                  - a_priori_vehicle_trans_vel_depression_angle;

  // Return the calculated scaling factor
  return std::cos(depress_angle_SONAR_wrt_trans_vel) / std::cos(depress_angle_edge_wrt_trans_vel);
}
