/********************************COPYRIGHT CHRIS D. MONACO, 2019**********************************/
#include <ros/ros.h>

#include <sonarodo/sonarodo.h>

int main(int argc, char* argv[]) {
    ros::init(argc, argv, "sonarodo");

    // Construct SONARODO ego-motion estimator object to estimate 2D Forward-Looking SONAR's
    // ego-motion
    Sonarodo sonarodo;

    ros::spin();

    return 0;
}
